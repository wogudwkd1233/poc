use crate::component::*;
use crate::misc::*;
use crate::asset::*;

use amethyst::prelude::*;
use amethyst::utils::application_root_dir;
use std::collections::HashMap;
use ron::de::from_str;
use std::fs::read_to_string;
use std::path::PathBuf;

fn add_settlement(path: PathBuf, world: &mut World) {
    let data = from_str::<AssetSettlementData>(&read_to_string(path).unwrap()).unwrap();

    let settlement = world
        .create_entity()
        .with(ComponentSettlement{ name: data.name.clone() })
        .build();

    for house_data in data.houses.iter() {
        let house = world
            .create_entity()
            .with(ComponentHouse{ name: house_data.name.clone() })
            .with(ComponentStockpile{ items: house_data.items.clone() })
            .with(ComponentLink{ to: settlement.clone() })
            .build();
        
        for pop_data in data.pops.iter() {
            if pop_data.to == house_data.name {
                world
                    .create_entity()
                    .with(ComponentPop{ variant: pop_data.variant.clone(), size: pop_data.size, status: MiscPopStatus::Normal, fill: 1.0, })
                    .with(ComponentLink{ to: house.clone() })
                    .build();
            }
        }
        
        for holding_data in data.holdings.iter() {
            if holding_data.to == house_data.name {
                let mut builder = world
                    .create_entity()
                    .with(ComponentHolding{ variant: holding_data.variant.0.clone(), status: MiscHoldingStatus::Normal, labor_fill: 0.0 })
                    .with(ComponentLink{ to: house.clone() });

                match holding_data.variant.0 {
                    MiscHolding::Farm => {
                        builder = builder
                            .with(ComponentCrop{ variant: holding_data.variant.1.clone(), water: 1.0, seed: 0.0, growth: 0.0});
                    }
                    MiscHolding::Workshop => {
                        builder = builder
                            .with(ComponentProduct{ variant: holding_data.variant.1.clone()});
                    }
                }

                builder.build();
            }
        }
    }
}

#[derive(Default)]
pub struct PocLoad;

impl SimpleState for PocLoad {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.insert(MiscTime{ month: 8, year: 1 });
        data.world.insert(MiscCurrentState::Paused);
        
        data.world.register::<ComponentSettlement>();

        let path = application_root_dir().unwrap().join("assets");
        let mut crops = HashMap::<MiscProduct, MiscCropData>::new();
        let mut products = HashMap::<MiscProduct, MiscProductData>::new();
        let mut pops = HashMap::<MiscPop, MiscPopData>::new();

        add_settlement(path.join("settlement").join("Ur.ron"), data.world);
        crops.insert(MiscProduct::Barley, from_str::<MiscCropData>(&read_to_string(path.join("crop").join("Barley.ron")).unwrap()).unwrap());
        products.insert(MiscProduct::Pottery, from_str::<MiscProductData>(&read_to_string(path.join("product").join("Pottery.ron")).unwrap()).unwrap());
        pops.insert(MiscPop::Farmer, from_str::<MiscPopData>(&read_to_string(path.join("pop").join("Farmer.ron")).unwrap()).unwrap());
        pops.insert(MiscPop::Artisan, from_str::<MiscPopData>(&read_to_string(path.join("pop").join("Artisan.ron")).unwrap()).unwrap());

        data.world.insert(crops);
        data.world.insert(products);
        data.world.insert(pops);
    }

    fn update(&mut self, _data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        Trans::Switch(Box::new(PocGame))
    }
}

pub struct PocGame;

impl SimpleState for PocGame {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.insert(MiscCurrentState::Running);

        println!("Loading Done!");
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        data.data.update(data.world);

        Trans::None
    }
}