use crate::component::*;
use crate::misc::*;

use amethyst::ecs::{ System, Join, ReadStorage, WriteStorage, Entities, Entity, Write, Read};
use std::collections::HashMap;

#[derive(Default)]
pub struct SystemGrowth;
impl<'a> System<'a> for SystemGrowth {
    type SystemData = (
        Read<'a, HashMap<MiscPop, MiscPopData>>,
        WriteStorage<'a, ComponentPop>,
    );

    fn run(&mut self, (pop_datas, mut pops): Self::SystemData) {
        for pop in (&mut pops).join() {
            let pop_data = pop_datas.get(&pop.variant).unwrap();

            if pop.fill > 1.0 {
                pop.size *= (pop.fill - 1.0) * pop_data.birth + 1.0;
            } else if pop.fill < 1.0 {
                pop.size *= (pop.fill - 1.0) * pop_data.death + 1.0;
            }
        }
    }
}

#[derive(Default)]
pub struct SystemEat;
impl<'a> System<'a> for SystemEat {
    type SystemData = (
        Read<'a, HashMap<MiscPop, MiscPopData>>,
        ReadStorage<'a, ComponentLink>,
        WriteStorage<'a, ComponentStockpile>,
        WriteStorage<'a, ComponentPop>,
    );

    fn run(&mut self, (pop_datas, links, mut stockpiles, mut pops): Self::SystemData) {
        let mut demands = HashMap::<Entity, HashMap<MiscItem, f32>>::new();
        let mut fills = HashMap::<Entity, HashMap<MiscItem, f32>>::new();

        for (link, mut pop) in (&links, &mut pops).join() {
            if !demands.contains_key(&link.to) {
                demands.insert(link.to.clone(), HashMap::new());
                fills.insert(link.to.clone(), HashMap::new());
            }

            pop.fill = match pop.status {
                MiscPopStatus::Half => 0.5,
                MiscPopStatus::Normal => 1.0,
                MiscPopStatus::Double => 2.0,
            };
            
            let pop_data = pop_datas.get(&pop.variant).unwrap();
            let demand = demands.get_mut(&link.to).unwrap();

            for (variant, val) in pop_data.ins.iter() {
                if let Some(valval) = demand.get_mut(variant) {
                    *valval += val * pop.size * pop.fill;
                } else {
                    demand.insert(variant.clone(), val * pop.size * pop.fill);
                }
            }
        }

        for (entity, demand) in demands.iter() {
            let stockpile = stockpiles.get_mut(*entity).unwrap();
            let fill = fills.get_mut(entity).unwrap();

            for (variant, val) in demand.iter() {
                if let Some(valval) = stockpile.items.get_mut(variant) {
                    if *valval >= *val {
                        fill.insert(variant.clone(), 1.0);
                        
                        *valval -= val;
                    } else {
                        fill.insert(variant.clone(), *valval / *val);

                        *valval = 0.0;
                    }
                } else {
                    fill.insert(variant.clone(), 0.0);
                }
            }
        }

        for (link, mut pop) in (&links, &mut pops).join() {
            let pop_data = pop_datas.get(&pop.variant).unwrap();
            let fill = fills.get(&link.to).unwrap();
            
            let mut sum = 0.0;
            let mut div = 0.0;

            for (variant, val) in pop_data.ins.iter() {
                sum += fill.get(variant).unwrap() * val;
                div += val;
            }

            pop.fill *= sum / div;
        }
    }
}

#[derive(Default)]
pub struct SystemLabor;
impl<'a> System<'a> for SystemLabor {
    type SystemData = (
        Entities<'a>,
        Read<'a, HashMap<MiscProduct, MiscCropData>>,
        Read<'a, HashMap<MiscProduct, MiscProductData>>,
        Read<'a, MiscTime>,
        ReadStorage<'a, ComponentCrop>,
        ReadStorage<'a, ComponentProduct>,
        ReadStorage<'a, ComponentLink>,
        ReadStorage<'a, ComponentHouse>,
        ReadStorage<'a, ComponentPop>,
        WriteStorage<'a, ComponentHolding>,
    );

    fn run(&mut self, (entities, crop_datas, product_datas, time, crops, products, links, houses, pops, mut holdings): Self::SystemData) {
        let mut holding_labors = HashMap::<Entity, HashMap<MiscPop, f32>>::new();
        let mut house_labors = HashMap::<Entity, HashMap<MiscPop, (f32, f32)>>::new();
        let mut labor_fills = HashMap::<Entity, HashMap<MiscPop, f32>>::new();

        for (entity, _) in (&entities, &holdings).join() {
            holding_labors.insert(entity.clone(), HashMap::new());
        }
        for (entity, _) in (&entities, &houses).join() {
            house_labors.insert(entity.clone(), HashMap::new());
            labor_fills.insert(entity.clone(), HashMap::new());
        }

        for (entity, link, holding) in (&entities, &links, &holdings).join() {
            let mut labors = HashMap::<MiscPop, f32>::new();

            match holding.variant {
                MiscHolding::Farm => {
                    let crop_data = crop_datas.get(&crops.get(entity).unwrap().variant).unwrap();

                    if crop_data.labor.contains_key(&time.month) {
                        labors = crop_data.labor.get(&time.month).unwrap().clone();
                    }
                }
                MiscHolding::Workshop => {
                    labors = product_datas.get(&products.get(entity).unwrap().variant).unwrap().labor.clone()
                }
            }

            if !labors.is_empty() {
                let holding_labor = holding_labors.get_mut(&entity).unwrap();
                let house_labor = house_labors.get_mut(&link.to).unwrap();

                let change = match holding.status {
                    MiscHoldingStatus::Half => 0.5,  
                    MiscHoldingStatus::Normal => 1.0,
                    MiscHoldingStatus::Double => 2.0,
                };

                for (variant, labor) in labors.iter() {
                    if let Some(val) = holding_labor.get_mut(variant) {
                        *val += labor * change;
                    } else {
                        holding_labor.insert(variant.clone(), labor * change);
                    }

                    if let Some(vals) = house_labor.get_mut(variant) {
                        vals.0 += labor * change;
                    } else {
                        house_labor.insert(variant.clone(), (labor * change, 0.0));
                    }
                }
            }
        }

        for (link, pop) in (&links, &pops).join() {
            let house_labor = house_labors.get_mut(&link.to).unwrap();

            if let Some(vals) = house_labor.get_mut(&pop.variant) {
                vals.1 += pop.size;
            } else {
                house_labor.insert(pop.variant.clone(), (0.0, pop.size));
            }
        }

        for (entity, _) in (&entities, &houses).join() {
            let house_labor = house_labors.get(&entity).unwrap();
            let labor_fill = labor_fills.get_mut(&entity).unwrap();

            for (variant, vals) in house_labor.iter() {
                if vals.1 >= vals.0 {
                    labor_fill.insert(variant.clone(), 1.0);
                } else {
                    labor_fill.insert(variant.clone(), vals.1 / vals.0);
                }
            }
        }

        for (entity, link, mut holding) in (&entities, &links, &mut holdings).join() {
            let holding_labor = holding_labors.get_mut(&entity).unwrap();
            let labor_fill = labor_fills.get(&link.to).unwrap();

            let mut fill = 0.0;
            let mut div = 0.0;

            for (variant, labor) in holding_labor.iter() {
                fill += labor_fill.get(variant).unwrap() * labor;
                div += labor;
            }

            if div == 0.0 {
                holding.labor_fill = -1.0;
            } else {
                holding.labor_fill = fill / div;

                match holding.status {
                    MiscHoldingStatus::Half => holding.labor_fill /= 2.0,  
                    MiscHoldingStatus::Normal => (),
                    MiscHoldingStatus::Double => holding.labor_fill *= 2.0,
                };
            }
        }
    }
}

#[derive(Default)]
pub struct SystemCrop;
impl<'a> System<'a> for SystemCrop {
    type SystemData = (
        Read<'a, HashMap<MiscProduct, MiscCropData>>,
        Read<'a, MiscTime>,
        ReadStorage<'a, ComponentLink>,
        ReadStorage<'a, ComponentHolding>,
        WriteStorage<'a, ComponentStockpile>,
        WriteStorage<'a, ComponentCrop>,
    );

    fn run(&mut self, (crop_datas, time, links, holdings, mut stockpiles, mut crops): Self::SystemData) {
        for (link, holding, mut crop) in (&links, &holdings, &mut crops).join() {
            if holding.labor_fill != -1.0 {
                let crop_data = crop_datas.get(&crop.variant).unwrap();

                if let Some(growth) = crop_data.growth.get(&time.month) {
                    let mut change: f32;

                    if crop.water <= crop_data.water.0 {
                        change = -1.0;
                    } else if crop.water >= crop_data.water.1 {
                        change = 1.0;
                    } else {
                        change = (((crop.water - crop_data.water.0) / (crop_data.water.1 - crop_data.water.0)) - 0.5) * 2.0
                    }

                    if change > 0.0 {
                        change *= growth.0;

                        if holding.labor_fill < 1.0 {
                            change *= holding.labor_fill;
                        }
                    } else if change < 0.0 {
                        change *= growth.1;
                    }

                    crop.growth += change;

                    if crop.growth > 1.0 {
                        crop.growth = 1.0;
                    } else if crop.growth < 0.0 {
                        crop.growth = 0.0;
                    }

                    if change < 0.0 {
                        crop.seed += change;

                        if crop.seed < 0.0 {
                            crop.seed = 0.0;
                        }
                    }
                } else if let Some(change) = crop_data.seed.get(&time.month) {
                    crop.growth = 0.0;
                    crop.seed += change * holding.labor_fill;

                    if crop.seed > 1.0 {
                        crop.seed = 1.0;
                    }
                } else if let Some(harvest) = crop_data.harvest.get(&time.month) {
                    let mut change = *harvest;
                    change *= holding.labor_fill;

                    if change > crop.seed {
                        change = crop.seed;
                    }

                    crop.seed -= change;
                    change *= crop.growth;

                    let stockpile = stockpiles.get_mut(link.to).unwrap();

                    for (variant, val) in crop_data.outs.iter() {
                        if let Some(valval) = stockpile.items.get_mut(variant) {
                            *valval += val * change;
                        } else {
                            stockpile.items.insert(variant.clone(), val * change);
                        }
                    }
                }
            }
        }
    }
}

#[derive(Default)]
pub struct SystemProduct;
impl<'a> System<'a> for SystemProduct {
    type SystemData = (
        Read<'a, HashMap<MiscProduct, MiscProductData>>,
        ReadStorage<'a, ComponentLink>,
        ReadStorage<'a, ComponentHolding>,
        ReadStorage<'a, ComponentProduct>,
        WriteStorage<'a, ComponentStockpile>,
    );

    fn run(&mut self, (product_datas, links, holdings, products, mut stockpiles): Self::SystemData) {
        for (link, holding, product) in (&links, &holdings, &products).join() {
            let product_data = product_datas.get(&product.variant).unwrap();
            let stockpile = stockpiles.get_mut(link.to).unwrap();
            let change: f32;

            if holding.labor_fill > 1.0 {
                change = (holding.labor_fill - 1.0) / 2.0 + 1.0;
            } else {
                change = holding.labor_fill;
            }

            for (variant, val) in product_data.outs.iter() {
                if let Some(valval) = stockpile.items.get_mut(variant) {
                    *valval += val * change;
                } else {
                    stockpile.items.insert(variant.clone(), val * change);
                }
            }
        }
    }
}

#[derive(Default)]
pub struct SystemTime;
impl<'a> System<'a> for SystemTime {
    type SystemData = Write<'a, MiscTime>;

    fn run(&mut self, mut time: Self::SystemData) {
        println!("{}-{}", time.year, time.month);

        time.month += 1;
        
        if time.month > 12 {
            time.month = 1;
            time.year += 1;
        }
    }
}