use crate::misc::*;

use serde::{ Serialize, Deserialize, };
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct AssetSettlementData {
    pub name: String,
    pub houses: Vec<AssetHouseData>,
    pub pops: Vec<AssetPopData>,
    pub holdings: Vec<AssetHoldingData>,
}

#[derive(Serialize, Deserialize)]
pub struct AssetHouseData {
    pub name: String,
    pub items: HashMap<MiscItem, f32>,
}

#[derive(Serialize, Deserialize)]
pub struct AssetPopData {
    pub to: String,
    pub variant: MiscPop,
    pub size: f32,
}

#[derive(Serialize, Deserialize)]
pub struct AssetHoldingData {
    pub to: String,
    pub variant: (MiscHolding, MiscProduct),
}