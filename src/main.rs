mod misc;
mod component;
mod system;
mod state;
mod asset;

use crate::state::*;
use crate::system::*;
use crate::misc::*;

extern crate ron;

use amethyst::{
    prelude::*,
    utils::application_root_dir,
};

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let game_data = GameDataBuilder::default()
        .with(SystemLabor::default().pausable(MiscCurrentState::Running), "Labor System", &[])
        .with(SystemCrop::default().pausable(MiscCurrentState::Running), "Crop System", &["Labor System"])
        .with(SystemProduct::default().pausable(MiscCurrentState::Running), "Product System", &["Labor System"])
        .with(SystemEat::default().pausable(MiscCurrentState::Running), "Eat System", &["Crop System"])
        .with(SystemGrowth::default().pausable(MiscCurrentState::Running), "Growth System", &["Eat System"])
        .with(SystemTime::default().pausable(MiscCurrentState::Running), "Time System", &["Growth System"]);

    let app_root = application_root_dir()?;
    let assets_dir = app_root.join("assets");

    let mut game = Application::new(assets_dir, PocLoad::default(), game_data)?;
    game.run();

    Ok(())
}