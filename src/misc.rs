use serde::{ Serialize, Deserialize, };
use std::hash::Hash;
use std::collections::HashMap;

#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum MiscItem {
    Food,
    Livingware,
}

#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum MiscPop {
    Farmer,
    Artisan,
}

#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum MiscHolding {
    Farm,
    Workshop,
}

#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum MiscProduct {
    Barley,
    Pottery,
}

pub enum MiscHoldingStatus {
    Half,
    Normal,
    Double,
}

pub enum MiscPopStatus {
    Half,
    Normal,
    Double,
}

#[derive(Default, Serialize, Deserialize)]
pub struct MiscCropData {
    pub outs: HashMap<MiscItem, f32>,
    pub water: (f32, f32),
    pub growth: HashMap<u32, (f32, f32)>,
    pub seed: HashMap<u32, f32>,
    pub harvest: HashMap<u32, f32>,
    pub labor: HashMap<u32, HashMap<MiscPop, f32>>,
}

#[derive(Default, Serialize, Deserialize)]
pub struct MiscProductData {
    pub outs: HashMap<MiscItem, f32>,
    pub ins: HashMap<MiscItem, f32>,
    pub labor: HashMap<MiscPop, f32>,
}

#[derive(Default, Serialize, Deserialize)]
pub struct MiscPopData {
    pub ins: HashMap<MiscItem, f32>,
    pub birth: f32,
    pub death: f32,
}

#[derive(Default)]
pub struct MiscTime {
    pub month: u32,
    pub year: u32,
}

#[derive(PartialEq)]
pub enum MiscCurrentState {
    Running,
    Paused,
}
impl Default for MiscCurrentState {
    fn default() -> Self {
        MiscCurrentState::Paused
    }
}