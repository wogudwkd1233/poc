use crate::misc::*;

use amethyst::{
    ecs::{Entity, Component, DenseVecStorage,},
};
use std::collections::HashMap;

pub struct ComponentStockpile {
    pub items: HashMap<MiscItem, f32>,
}
impl Component for ComponentStockpile {
    type Storage = DenseVecStorage<Self>;
}

pub struct ComponentLink {
    pub to: Entity,
}
impl Component for ComponentLink {
    type Storage = DenseVecStorage<Self>;
}

pub struct ComponentHouse {
    pub name: String,
}
impl Component for ComponentHouse {
    type Storage = DenseVecStorage<Self>;
}

pub struct ComponentSettlement {
    pub name: String,
}
impl Component for ComponentSettlement {
    type Storage = DenseVecStorage<Self>;
}

pub struct ComponentHolding {
    pub variant: MiscHolding,
    pub status: MiscHoldingStatus,
    pub labor_fill: f32,
}
impl Component for ComponentHolding {
    type Storage = DenseVecStorage<Self>;
}

pub struct ComponentPop {
    pub variant: MiscPop,
    pub status: MiscPopStatus,
    pub size: f32,
    pub fill: f32,
}
impl Component for ComponentPop {
    type Storage = DenseVecStorage<Self>;
}

pub struct ComponentCrop {
    pub variant: MiscProduct,
    pub water: f32,
    pub seed: f32,
    pub growth: f32,
}
impl Component for ComponentCrop {
    type Storage = DenseVecStorage<Self>;
}

pub struct ComponentProduct {
    pub variant: MiscProduct,
}
impl Component for ComponentProduct {
    type Storage = DenseVecStorage<Self>;
}